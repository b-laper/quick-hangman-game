const Header = (): JSX.Element => {
  return (
    <>
      <h1>Hangman</h1>
      <p>Figure out the hidden word ! - Enter a letter</p>
    </>
  );
};

export default Header;
