interface WordProps {
  selectedWord: string;
  correctLetters: string[];
}

const Word = ({ selectedWord, correctLetters }: WordProps): JSX.Element => {
  return (
    <div className="word" id="word">
      {selectedWord.split("").map((letter: string, index: number) => {
        return (
          <span className="letter" key={index}>
            {correctLetters.includes(letter) ? letter : ""}
          </span>
        );
      })}
    </div>
  );
};

export default Word;
