import { checkWin } from "../helpers/helpers";
import { useEffect } from "react";
interface PopupProps {
  correctLetters: string[];
  selectedWord: string;
  wrongLetters: string[];
  setPlayable: any;
  playAgain: () => void;
}
const Popup = (props: PopupProps): JSX.Element => {
  let finalMessage = "";
  let finalMessageRevealWord = "";
  let playable = true;

  if (
    checkWin(props.correctLetters, props.wrongLetters, props.selectedWord) ===
    "win"
  ) {
    finalMessage = "Congratulations! You won! 😃";
    playable = false;
  } else if (
    checkWin(props.correctLetters, props.wrongLetters, props.selectedWord) ===
    "lose"
  ) {
    finalMessage = "Unfortunately you lost. 😕";
    finalMessageRevealWord = `...the word was: ${props.selectedWord}`;
    playable = false;
  }

  useEffect(() => {
    props.setPlayable(playable);
  });

  return (
    <div
      className="popup-container"
      style={finalMessage !== "" ? { display: "flex" } : {}}
    >
      <div className="popup">
        <h2>{finalMessage}</h2>
        <h3>{finalMessageRevealWord}</h3>
        <button onClick={props.playAgain}>Play Again</button>
      </div>
    </div>
  );
};

export default Popup;
