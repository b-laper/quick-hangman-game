import { useState, useEffect } from "react";
import Header from "./components/Header";
import Figure from "./components/Figure";
import Notification from "./components/Notification";
import WrongLetters from "./components/WrongLetters";
import Word from "./components/Word";
import { showNotification as show } from "./helpers/helpers";
import Popup from "./components/Popup";
import "./App.css";

const words = [
  "react",
  "application",
  "google",
  "programming",
  "interface",
  "union",
  "typescript",
];
let selectedWord = words[Math.floor(Math.random() * words.length)];

const App = (): JSX.Element => {
  const [playable, setPlayable] = useState<boolean>(true);
  const [correctLetters, setCorrectLetters] = useState<string[]>([]);
  const [wrongLetters, setWrongLetters] = useState<string[]>([]);
  const [showNotification, setShowNotification] = useState<boolean>(false);

  useEffect(() => {
    const handleKeydown = (event: any): void => {
      const { key, keyCode } = event;
      if (playable && keyCode >= 65 && keyCode <= 90) {
        const letter = key.toLowerCase();
        if (selectedWord.includes(letter)) {
          if (!correctLetters.includes(letter)) {
            setCorrectLetters((currentLetters) => [...currentLetters, letter]);
          } else {
            show(setShowNotification);
          }
        } else {
          if (!wrongLetters.includes(letter)) {
            setWrongLetters((currentLetters) => [...currentLetters, letter]);
          } else {
            show(setShowNotification);
          }
        }
      }
    };
    window.addEventListener("keydown", handleKeydown);

    return () => window.removeEventListener("keydown", handleKeydown);
  }, [correctLetters, wrongLetters, playable]);

  const playAgain = () => {
    setPlayable(true);
    setCorrectLetters([]);
    setWrongLetters([]);

    const random = Math.floor(Math.random() * words.length);
    selectedWord = words[random];
  };

  return (
    <>
      <Header />
      <div className="game-cointainer">
        <Figure wrongLetters={wrongLetters} />
        <WrongLetters wrongLetters={wrongLetters} />
        <Word correctLetters={correctLetters} selectedWord={selectedWord} />
      </div>
      <Popup
        correctLetters={correctLetters}
        selectedWord={selectedWord}
        wrongLetters={wrongLetters}
        setPlayable={setPlayable}
        playAgain={playAgain}
      />
      <Notification showNotification={showNotification} />
    </>
  );
};

export default App;
